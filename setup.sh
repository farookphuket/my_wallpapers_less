#!/bin/bash


# check if folder exit
pic_dir=~/Pictures/wallpapers 


if [ -d "$pic_dir" ]; then
    cp -r ~/my_wallpapers_less/Wallpapers/ ~/Pictures/wallpapers 
fi 
mkdir -p ~/Pictures/wallpapers 
cp -r ~/my_wallpapers_less/Wallpapers/ ~/Pictures/wallpapers 


function goodbye(){
    d_title="Success your wallpapers has been copied"
    b_title="\Z2 Success copied new wallpapers!"
    MSG="\Z7 dear $USER your new wallpapers has been copied \n
    \Z3 image from `~/my_wallpapers_less/Wallpapers/ -> ~/Pictures/wallpapers` \n
    please use the file manage program to review it. \n
    \Z1 Thank you $USER for using the script \n
    \Z3 Farook."
    dialog --clear \
        --colors \
        --title "$d_title" \
        --backtitle "$b_title" \
        --msgbox "$MSG" \
        14 46
}

goodbye

rm -rf ~/Pictures/wallpapers/setup.sh
rm -rf ~/Pictures/wallpapers/.git
rm -rf ~/Pictures/wallpapers/README.md

# delete this dir 
rm -rf ~/my_wallpapers_less 
